package org.jboss.as.quickstarts.bpmprocess;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.avalonhc.as.facts.ClaimObj;
import com.avalonhc.as.facts.ClaimRequest;
import com.avalonhc.as.facts.EvaluateLabClaim;
import com.avalonhc.as.facts.EvaluateLabClaimResponse;
import com.avalonhc.as.facts.ResponseHeaderData;
import com.avalonhc.as.facts.ResponseLineData;
import com.avalonhc.as.facts.SecondaryCodes;
import com.avalonhc.as.facts.ServiceLine;

import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

import java.util.logging.Logger; 


public class DemographicsMain {
	
	private final static Logger LOGGER = Logger.getLogger(DemographicsMain.class.getName());  
		public EvaluateLabClaimResponse start(EvaluateLabClaim evaluateLabClaim) throws ParseException {

			String path = System.getProperty("javaDefaultValue"); 
			 
			 LOGGER.info("Process Main environment variable::"+path);		
			KieServices ks = KieServices.Factory.get();
			KieContainer kContainer = ks.getKieClasspathContainer();
			KieSession ksession = kContainer.newKieSession("ksession-process");
			
			Map<String, Object> params = new HashMap<String, Object>();
	         SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	         ClaimRequest claimRequest =new ClaimRequest();
	         LOGGER.info("healthpland:::"+evaluateLabClaim.getRequestHeader().getHealthPlanId());
	         if(null!= evaluateLabClaim.getRequestHeader().getHealthPlanId() && !"".equals(evaluateLabClaim.getRequestHeader().getHealthPlanId())){
	     		claimRequest.setHealthPlanId(evaluateLabClaim.getRequestHeader().getHealthPlanId());
	     		LOGGER.info("Health plan:::"+evaluateLabClaim.getRequestHeader().getHealthPlanId());
	     		}
	     	
	    	        	
	         if(null!=evaluateLabClaim.getRequestLine().get(0).getProcedureCode() && !"".equals(evaluateLabClaim.getRequestLine().get(0).getProcedureCode())){
		 		   claimRequest.setProcedureCode(evaluateLabClaim.getRequestLine().get(0).getProcedureCode());
		 		   LOGGER.info("procedure code:::"+ evaluateLabClaim.getRequestLine().get(0).getProcedureCode());
		 		} 
	    	         
	         if(null!=evaluateLabClaim.getRequestLine().get(0).getFromDateOfService() && !"".equals(evaluateLabClaim.getRequestLine().get(0).getFromDateOfService())){
		       	claimRequest.setFromDateOfService(evaluateLabClaim.getRequestLine().get(0).getFromDateOfService().toGregorianCalendar().getTime());
	        	 //claimRequest.setFromDateOfService(evaluateLabClaim.getRequestLine().get(0).getFromDateOfService());
		        	 LOGGER.info("From Date::"+evaluateLabClaim.getRequestLine().get(0).getFromDateOfService());
		         }
	    	      
		        	 LOGGER.info("DOB::"+evaluateLabClaim.getRequestHeader().getPatientDateOfBirth());
		        	 LOGGER.info("primaryDiagnosis:::"+evaluateLabClaim.getRequestHeader().getPrimaryDiagnosisCode());
		        	 
		        	 if(null!=evaluateLabClaim.getRequestLine().get(0).getPlaceOfService() && !"".equals(evaluateLabClaim.getRequestLine().get(0).getPlaceOfService())){
		 	        	   claimRequest.setPlaceOfService(evaluateLabClaim.getRequestLine().get(0).getPlaceOfService());
		    	        	 LOGGER.info("PlaceOfService::::"+evaluateLabClaim.getRequestLine().get(0).getPlaceOfService());
		 	        	  }
	    	        	 //setting PrimaryDiagnosisCode and DiagnosisCode in List for comparing with single column value in decision table
		        	 claimRequest.setError("No");
		        	 List<String> diagnosisCodesList =new ArrayList<String>();
		        	 
		        	 int DiagnosisCodePointersCount =Integer.parseInt(evaluateLabClaim.getRequestLine().get(0).getDiagnosisCodePointers().get(0));
		        	 int count = evaluateLabClaim.getRequestHeader().getDiagnosisCodes().size();
		        	
		        	 if(null== evaluateLabClaim.getRequestHeader().getPrimaryDiagnosisCode() || "".equals(evaluateLabClaim.getRequestHeader().getPrimaryDiagnosisCode())){
		        	 claimRequest.setDiagnosisCodePointers(DiagnosisCodePointersCount);
		        	 }
		        	 
		        	 
	    	        	 LOGGER.info("Step 1 diagnosisCodesList ---->");
	    	        	 
	    	        	
	    	        		if(null!= evaluateLabClaim.getRequestHeader().getPrimaryDiagnosisCode() && !"".equals(evaluateLabClaim.getRequestHeader().getPrimaryDiagnosisCode())){
	    	      	        	 diagnosisCodesList.add(evaluateLabClaim.getRequestHeader().getPrimaryDiagnosisCode());
	    	      	        	 claimRequest.setPrimaryDiagnosisCode(evaluateLabClaim.getRequestHeader().getPrimaryDiagnosisCode());
	    	      	        	}
	    	        		LOGGER.info("in for loop diagnosiscode list"+evaluateLabClaim.getRequestHeader().getDiagnosisCodes());
		  	        		LOGGER.info("in for loop the value of count1"+count);
	    	  	        	if(null== evaluateLabClaim.getRequestHeader().getDiagnosisCodes() || "".equals(evaluateLabClaim.getRequestHeader().getDiagnosisCodes()) || "?".equals(evaluateLabClaim.getRequestHeader().getDiagnosisCodes())){
	    	  	        		LOGGER.info("in for loop diagnosiscode list"+evaluateLabClaim.getRequestHeader().getDiagnosisCodes());
	    	  	        		LOGGER.info("in for loop the value of count"+count);
	    	  	        		for (int i = 0; i < count; i++) {
	    	  	        			LOGGER.info("in for loop the value of i"+i);
	    	  	        			diagnosisCodesList.add(evaluateLabClaim.getRequestHeader().getDiagnosisCodes().get(i));
	    	  	        			LOGGER.info("in for loop diagnosiscode list"+evaluateLabClaim.getRequestHeader().getDiagnosisCodes().get(i));
	    	  	      		}
	    	  	       
	    	  	        	 
	    	  	        	}
	    	        		 LOGGER.info("Step 1 diagnosisCodesList ---->");
	    	      	        	if(null!= evaluateLabClaim.getRequestHeader().getDiagnosisCodes() && !"".equals(evaluateLabClaim.getRequestHeader().getDiagnosisCodes())){
	    	      	        	// diagnosisCodesList.add(evaluateLabClaim.getRequestHeader().getDiagnosisCodes());
	    	      	        		for(int  i=0;i<evaluateLabClaim.getRequestHeader().getDiagnosisCodes().size();i++){
	    	      	        			LOGGER.info("diagnosisCodesList ---->"+evaluateLabClaim.getRequestHeader().getDiagnosisCodes().get(i));
	    	      	        			diagnosisCodesList.add(evaluateLabClaim.getRequestHeader().getDiagnosisCodes().get(i));
	    	      	        		}
	    	      	        		
	    	      	        	}
	    	      	        	LOGGER.info("diagnosisCodesList ---->"+diagnosisCodesList); 
	    	      	        	claimRequest.setDiagnosisCodesList(diagnosisCodesList);
	    	     	        	 
	    	     	        	if(null!=evaluateLabClaim.getRequestHeader().getPatientGenderCode() && !"".equals(evaluateLabClaim.getRequestHeader().getPatientGenderCode())){
	    	        	        	 LOGGER.info("Gender::::"+evaluateLabClaim.getRequestHeader().getPatientGenderCode());
	    	        	        	 claimRequest.setPatientGenderCode(evaluateLabClaim.getRequestHeader().getPatientGenderCode());
	    	        	        	}
	        	        	  
	     	        	//Sample Response creation for global flow

	     	        	ResponseHeaderData responseHeaderData = new ResponseHeaderData();
	     	        	responseHeaderData.setClaimNumber("12345");
	     	        	responseHeaderData.setCeTransactionId("100");
	     	        	EvaluateLabClaimResponse evaluateLabClaimResponse = new EvaluateLabClaimResponse();
	     	        	SecondaryCodes secondaryCodes = new SecondaryCodes();
	     	        	ResponseLineData responseLineData =new ResponseLineData();
	     	        	evaluateLabClaimResponse.setResponseHeader(responseHeaderData);
	     	        	
	    	        	 
	    	        	 params.put("claimReq", claimRequest);
	    	        	 params.put("evaluateLabClaimResponse", evaluateLabClaimResponse);
	    	        	 params.put("secondaryCodes", secondaryCodes);
	    	        	 params.put("responseLineData", responseLineData);

			            ksession.startProcess("ClaimEditProcess.DemographicsSubProces", params);
			            ksession.insert(evaluateLabClaim);
			            ksession.fireAllRules();	
			            
			          
			            ksession.dispose();
			            LOGGER.info("returninging responseMap map --->"+evaluateLabClaimResponse);
			            
			            
			           

			            return evaluateLabClaimResponse;
	}


}