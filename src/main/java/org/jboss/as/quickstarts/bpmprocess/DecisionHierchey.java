package org.jboss.as.quickstarts.bpmprocess;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

import com.avalonhc.as.facts.ClaimRequest;
import com.avalonhc.as.facts.EvaluateLabClaim;
import com.avalonhc.as.facts.EvaluateLabClaimResponse;
import com.avalonhc.as.facts.ResponseHeaderData;
import com.avalonhc.as.facts.ResponseLineData;
import com.avalonhc.as.facts.SecondaryCodes;
import java.util.logging.Logger; 

public class DecisionHierchey {
	
	private final static Logger LOGGER = Logger.getLogger(DecisionHierchey.class.getName());  
	
	public static EvaluateLabClaimResponse start(EvaluateLabClaim evaluateLabClaim) throws ParseException {

		String path = System.getProperty("javaDefaultValue"); 
		 
		 LOGGER.info("Process Main environment variable::"+path);		
		KieServices ks = KieServices.Factory.get();
		KieContainer kContainer = ks.getKieClasspathContainer();
		KieSession ksession = kContainer.newKieSession("ksession-process");
		
		Map<String, Object> params = new HashMap<String, Object>();
         SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
         ClaimRequest claimRequest =new ClaimRequest();
           	
     	        	ResponseHeaderData responseHeaderData = new ResponseHeaderData();

     	        	EvaluateLabClaimResponse evaluateLabClaimResponse = new EvaluateLabClaimResponse();
     	        	SecondaryCodes secondaryCodes = new SecondaryCodes();
     	        	ResponseLineData responseLineData =new ResponseLineData();
     	        	
     	        	
     	        	evaluateLabClaimResponse.setResponseHeader(responseHeaderData);
     	        	evaluateLabClaimResponse.setResponseHeader(responseHeaderData);
     	        	
     	        	
     	        	 //hardcoded later it will be removed

		            SecondaryCodes secondaryCodes1=new SecondaryCodes();

		            secondaryCodes1.setSecondaryDecisionCode("A01R");

		            secondaryCodes1.setSecondaryPolicyTag("Policytag for E&I");

		            secondaryCodes1.setSecondaryPolicyNecessityCriteria("Policy ncc");

		            SecondaryCodes secondaryCodes2=new SecondaryCodes();

		            secondaryCodes2.setSecondaryDecisionCode("D02R");

		            secondaryCodes2.setSecondaryPolicyTag("Policag for E&I");

		            secondaryCodes2.setSecondaryPolicyNecessityCriteria("Polcc.cre for E&I");
		            ArrayList<SecondaryCodes> ListOfSecondaryCodes = new  ArrayList<SecondaryCodes>();
		            ListOfSecondaryCodes.add(secondaryCodes1);

		            ListOfSecondaryCodes.add(secondaryCodes2);

		           String healthplanId ="401";   
		           //end hardcoded value 
    	        	 
    	        	 params.put("claimReq", claimRequest);
    	        	 params.put("evaluateLabClaimResponse", evaluateLabClaimResponse);
    	        	 params.put("secondaryCodes", secondaryCodes);
    	        	 params.put("responseLineData", responseLineData);
    	        	 params.put("ListOfSecondaryCodes", ListOfSecondaryCodes);
    	        	 params.put("healthplanId", healthplanId);

    	        	LOGGER.info("Start calling claim.DecisionHierarchy from java");
		            ksession.startProcess("ClaimEditProcess.DecisionHierarchy", params);
		            ksession.insert(evaluateLabClaim);
		            ksession.fireAllRules();	
		            
		            ksession.dispose();
		            LOGGER.info("returninging responseMap map --->"+evaluateLabClaimResponse);
		            return evaluateLabClaimResponse;
	
	}
/* public static void main (String args[]) throws ParseException{
		LOGGER.info("Start Main Method ");
		EvaluateLabClaim evaluateLabClaim = new  EvaluateLabClaim();
		EvaluateLabClaimResponse res=start(evaluateLabClaim);
		LOGGER.info("ENd of Main Method ");
		
	}*/
	
}