


package org.jboss.as.quickstarts.bpmprocess;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

import com.avalonhc.as.facts.ClaimObj;
import com.avalonhc.as.facts.ClaimRequest;
import com.avalonhc.as.facts.EvaluateLabClaimResponse;
import com.avalonhc.as.facts.ResponseLineData;
import com.avalonhc.as.facts.SecondaryCodes;

import java.util.logging.Logger; 

public class ProcedureCodeLookupMain  {

	private final static Logger LOGGER = Logger.getLogger(ProcedureCodeLookupMain.class.getName()); 
	public Map<String,String> start(ClaimObj p) throws ParseException {
		String path = System.getProperty("javaDefaultValue"); 
		 
		 LOGGER.info("Process Main environment variable::"+path);		
		KieServices ks = KieServices.Factory.get();
		KieContainer kContainer = ks.getKieClasspathContainer();

		KieSession ksession = kContainer.newKieSession("ksession-process");
		
		
		Map<String, Object> params = new HashMap<String, Object>();

         SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
         ClaimRequest claimRequest =new ClaimRequest();
         LOGGER.info("healthpland"+p.getHealthPlanId());
         if(null!= p.getHealthPlanId() && !"".equals(p.getHealthPlanId())){
     		claimRequest.setHealthPlanId(p.getHealthPlanId());
     		LOGGER.info(p.getHealthPlanId());
     		}
     		LOGGER.info("Hello");

    	         LOGGER.info("Hello");
    	        	
    	         if(null!=p.getServiceLines().get(0).getProcedureCode() && !"".equals(p.getServiceLines().get(0).getProcedureCode())){
    	 		   claimRequest.setProcedureCode(p.getServiceLines().get(0).getProcedureCode());
    	 		   LOGGER.info("proceceur"+ p.getServiceLines().get(0).getProcedureCode());
    	 		} 
    	         
    	         if(null!=p.getServiceLines().get(0).getFromDateOfService() && !"".equals(p.getServiceLines().get(0).getFromDateOfService())){
    	        	claimRequest.setFromDateOfService(sdf.parse(p.getServiceLines().get(0).getFromDateOfService())); 
    	        	 LOGGER.info("date::"+p.getServiceLines().get(0).getFromDateOfService());
    	         }
    	        	 LOGGER.info("DOB::"+p.getPatientDOB());
    	        	 LOGGER.info("primaryDiagnosis"+p.getPrimaryDiagnosisCode());
    	        	 
    	        	 if(null!=p.getServiceLines().get(0).getPlaceOfService() && !"".equals(p.getServiceLines().get(0).getPlaceOfService())){
    	 	        	   claimRequest.setPlaceOfService(p.getServiceLines().get(0).getPlaceOfService());
    	    	        	 LOGGER.info("proceceur"+p.getServiceLines().get(0).getPlaceOfService());
    	 	        	  }
    	        	
     	        	 List<String> diagnosisCodesList =new ArrayList<String>();
     	        	if(null!= p.getPrimaryDiagnosisCode() && !"".equals(p.getPrimaryDiagnosisCode())){
     	        	 diagnosisCodesList.add(p.getPrimaryDiagnosisCode());
     	        	}
     	        	if(null!= p.getDiagnosisCodes() && !"".equals(p.getDiagnosisCodes())){
     	        	 diagnosisCodesList.add(p.getDiagnosisCodes());
     	        	}
     	        	 claimRequest.setDiagnosisCodesList(diagnosisCodesList);
     	        	 
     	        	if(null!=p.getPatientGenderCode() && !"".equals(p.getPatientGenderCode())){
       	        	 LOGGER.info("Gender:"+p.getPatientGenderCode());
       	        	 claimRequest.setPatientGenderCode(p.getPatientGenderCode());
       	        	}
     	
    	        	 
    	        	 
    	        	params.put("claimReq", claimRequest);

		            ksession.startProcess("ClaimProcessings.PxcodeLookupSubProcess", params);
		            ksession.insert(p);
		            ksession.fireAllRules();	
		            String response=claimRequest.getDecision();
		            String associatedMPolicy=claimRequest.getAssociatedMPolicy();
		            String associatedMnc=claimRequest.getAssociatedMnc();
		            Map<String,String>  responseMap = new HashMap<String, String>();
		            responseMap.put("response", response);
		            responseMap.put("associatedMPolicy", associatedMPolicy);
		            responseMap.put("associatedMnc", associatedMnc);
		            ksession.dispose();
		            LOGGER.info("returninging responseMap map --->"+responseMap);
		            
		            
		            
		           

		return responseMap;
	}


}