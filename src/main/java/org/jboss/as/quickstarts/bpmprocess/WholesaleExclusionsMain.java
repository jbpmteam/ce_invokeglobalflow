


package org.jboss.as.quickstarts.bpmprocess;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

import com.avalonhc.as.facts.ClaimObj;
import com.avalonhc.as.facts.ClaimRequest;

import java.util.logging.Logger; 

public class WholesaleExclusionsMain  {
	
	
	private final static Logger LOGGER = Logger.getLogger(WholesaleExclusionsMain.class.getName()); 
	public Map<String,String> start(ClaimObj p) throws ParseException {
		String path = System.getProperty("javaDefaultValue"); 
		 
		 LOGGER.info("Process Main environment variable::"+path);		
		KieServices ks = KieServices.Factory.get();
		KieContainer kContainer = ks.getKieClasspathContainer();

		KieSession ksession = kContainer.newKieSession("ksession-process");
		
		Map<String, Object> params = new HashMap<String, Object>();
		
         SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
         ClaimRequest claimRequest =new ClaimRequest();
         LOGGER.info("healthpland"+p.getHealthPlanId());
      
         if(null!= p.getHealthPlanId() && !"".equals(p.getHealthPlanId())){
     		claimRequest.setHealthPlanId(p.getHealthPlanId());
     		LOGGER.info(p.getHealthPlanId());
     		}
      
         if(null!=p.getServiceLines().get(0).getFromDateOfService() && !"".equals(p.getServiceLines().get(0).getFromDateOfService())){
	        	claimRequest.setFromDateOfService(sdf.parse(p.getServiceLines().get(0).getFromDateOfService())); 
	        	 LOGGER.info("date::"+p.getServiceLines().get(0).getFromDateOfService());
	         }
         if(null!=p.getServiceLines().get(0).getEffectiveEndDate() && !"".equals(p.getServiceLines().get(0).getEffectiveEndDate())){
	        	claimRequest.setEffectiveEndDate(sdf.parse(p.getServiceLines().get(0).getEffectiveEndDate())); 
	        	 LOGGER.info("date::"+p.getServiceLines().get(0).getEffectiveEndDate());
	         }
      
         if(null!= p.getHealthPlanGroupId() && !"".equals(p.getHealthPlanGroupId())){
      		claimRequest.setHealthPlanGroupId(p.getHealthPlanGroupId());
      		LOGGER.info(p.getHealthPlanGroupId());
      		}
      
         if(null!= p.getRenderingProviderNpi() && !"".equals(p.getRenderingProviderNpi())){
      		claimRequest.setRenderingProviderNpi(p.getRenderingProviderNpi());
      		LOGGER.info(p.getRenderingProviderNpi());
      		}
         if(null!= p.getBillingProviderNpi() && !"".equals(p.getBillingProviderNpi())){
       		claimRequest.setBillingProviderNpi(p.getBillingProviderNpi());
       		LOGGER.info(p.getBillingProviderNpi());
       		}
         if(null!=p.getServiceLines().get(0).getPlaceOfService() && !"".equals(p.getServiceLines().get(0).getPlaceOfService())){
       	   claimRequest.setPlaceOfService(p.getServiceLines().get(0).getPlaceOfService());
	        	 LOGGER.info("proceceur"+p.getServiceLines().get(0).getPlaceOfService());
       	  }
         if(null!= p.getLineOfBusiness() && !"".equals(p.getLineOfBusiness())){
       		claimRequest.setLineOfBusiness(p.getLineOfBusiness());
       		LOGGER.info(p.getLineOfBusiness());
       		}
         if(null!= p.getBlueCardCode() && !"".equals(p.getBlueCardCode())){
        		claimRequest.setBlueCardCode(p.getBlueCardCode());
        		LOGGER.info(p.getBlueCardCode());
        		}
         if(null!=p.getServiceLines().get(0).getInNetworkIndicator() && !"".equals(p.getServiceLines().get(0).getInNetworkIndicator())){
	        	   claimRequest.setInNetworkIndicator(p.getServiceLines().get(0).getInNetworkIndicator());
	        	 LOGGER.info("InNetworkIndicator-->"+p.getServiceLines().get(0).getInNetworkIndicator());
	        	  }
         if(null!= p.getIdCardNumber() && !"".equals(p.getIdCardNumber())){
        		claimRequest.setIdCardNumber(p.getIdCardNumber());
        		LOGGER.info(p.getIdCardNumber());
        		}
         if(null!=p.getServiceLines().get(0).getOtherClaimEditorIndicator() && !"".equals(p.getServiceLines().get(0).getOtherClaimEditorIndicator())){
      	   claimRequest.setOtherClaimEditorIndicator(p.getServiceLines().get(0).getOtherClaimEditorIndicator());
      	 LOGGER.info("InNetworkIndicator-->"+p.getServiceLines().get(0).getOtherClaimEditorIndicator());
      	  }
     
         if(null!=p.getServiceLines().get(0).getOtherClaimEditorIndicator() && !"".equals(p.getServiceLines().get(0).getOtherClaimEditorIndicator())){
        	   claimRequest.setOtherClaimEditorIndicator(p.getServiceLines().get(0).getOtherClaimEditorIndicator());
        	 LOGGER.info("OtherClaimEditorIndicator-->"+p.getServiceLines().get(0).getOtherClaimEditorIndicator());
        	  }
         if(null!=p.getServiceLines().get(0).getProcedureCode() && !"".equals(p.getServiceLines().get(0).getProcedureCode())){
	 		   claimRequest.setProcedureCode(p.getServiceLines().get(0).getProcedureCode());
	 		   LOGGER.info("proceceur"+ p.getServiceLines().get(0).getProcedureCode());
	 		} 
    	          	 
    	        	params.put("claimReq", claimRequest);

		            ksession.startProcess("ClaimEditProcess.WholesaleExclusionsSubProcess", params);
		            ksession.insert(p);
		            ksession.fireAllRules();	
		            String response=claimRequest.getDecision();
		            Map<String,String>  responseMap = new HashMap<String, String>();
		            responseMap.put("response", response);
		            ksession.dispose();
		            LOGGER.info("returninging responseMap map --->"+responseMap);

		return responseMap;
	}

}