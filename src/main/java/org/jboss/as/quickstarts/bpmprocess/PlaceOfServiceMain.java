package org.jboss.as.quickstarts.bpmprocess;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

import com.avalonhc.as.facts.ClaimObj;
import com.avalonhc.as.facts.ClaimRequest;

import java.util.logging.Logger; 

public class PlaceOfServiceMain {
	
	private final static Logger LOGGER = Logger.getLogger(PlaceOfServiceMain.class.getName()); 
	public Map<String,String> start(ClaimObj p) throws ParseException {
		String path = System.getProperty("javaDefaultValue"); 
		 
		 System.out.println("Process Main environment variable::"+path);		
		KieServices ks = KieServices.Factory.get();
		KieContainer kContainer = ks.getKieClasspathContainer();
		
		KieSession ksession = kContainer.newKieSession("ksession-process");
		
	
		Map<String, Object> params = new HashMap<String, Object>();

         SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
         ClaimRequest claimRequest =new ClaimRequest();
         System.out.println("healthpland"+p.getHealthPlanId());
         if(null!= p.getHealthPlanId() && !"".equals(p.getHealthPlanId())){
     		claimRequest.setHealthPlanId(p.getHealthPlanId());
     		System.out.println(p.getHealthPlanId());
     		}
     		System.out.println("Hello");

    	         System.out.println("Hello");
    	        	
    	         if(null!=p.getServiceLines().get(0).getProcedureCode() && !"".equals(p.getServiceLines().get(0).getProcedureCode())){
    	 		   claimRequest.setProcedureCode(p.getServiceLines().get(0).getProcedureCode());
    	 		   System.out.println("proceceur"+ p.getServiceLines().get(0).getProcedureCode());
    	 		} 
    	         
    	         if(null!=p.getServiceLines().get(0).getFromDateOfService() && !"".equals(p.getServiceLines().get(0).getFromDateOfService())){
    	        	claimRequest.setFromDateOfService(sdf.parse(p.getServiceLines().get(0).getFromDateOfService())); 
    	        	 System.out.println("date::"+p.getServiceLines().get(0).getFromDateOfService());
    	         }
    	        	 System.out.println("DOB::"+p.getPatientDOB());
    	        	 System.out.println("primaryDiagnosis"+p.getPrimaryDiagnosisCode());
    	        	 
    	        	 if(null!=p.getServiceLines().get(0).getPlaceOfService() && !"".equals(p.getServiceLines().get(0).getPlaceOfService())){
    	 	        	   claimRequest.setPlaceOfService(p.getServiceLines().get(0).getPlaceOfService());
    	    	        	 System.out.println("proceceur"+p.getServiceLines().get(0).getPlaceOfService());
    	 	        	  }
    	        	 //setting PrimaryDiagnosisCode and DiagnosisCode in List for comparing with single column value in decision table
    	        	 List<String> diagnosisCodesList =new ArrayList<String>();
      	        	if(null!= p.getPrimaryDiagnosisCode() && !"".equals(p.getPrimaryDiagnosisCode())){
      	        	 diagnosisCodesList.add(p.getPrimaryDiagnosisCode());
      	        	}
      	        	if(null!= p.getDiagnosisCodes() && !"".equals(p.getDiagnosisCodes())){
      	        	 diagnosisCodesList.add(p.getDiagnosisCodes());
      	        	}
      	        	 claimRequest.setDiagnosisCodesList(diagnosisCodesList);
     	        	 
     	        	if(null!=p.getPatientGenderCode() && !"".equals(p.getPatientGenderCode())){
        	        	 System.out.println("Gender:"+p.getPatientGenderCode());
        	        	 claimRequest.setPatientGenderCode(p.getPatientGenderCode());
        	        	}
        	        	  
     	        	 //Passing the DOB to utility class to convert into age in years
     	        	 /*SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
     	   		     Date birthDate = simpleDateFormat.parse(p.getPatientDOB()); 
    	        	 int calculatedAge=DateUtility.calculateAge(birthDate);
    	        	 claimRequest.setCalculatedAge(calculatedAge);*/
    	        	 
    	        	 
    	        	 params.put("claimReq", claimRequest);

		            ksession.startProcess("ClaimEditProcess.PlaceOfServiceSubProcess", params);
		            ksession.insert(p);
		            ksession.fireAllRules();	
		            String response=claimRequest.getDecision();
		            String associatedMPolicy=claimRequest.getAssociatedMPolicy();
		            String associatedMnc=claimRequest.getAssociatedMnc();
		            Map<String,String>  responseMap = new HashMap<String, String>();
		            responseMap.put("response", response);
		            responseMap.put("associatedMPolicy", associatedMPolicy);
		            responseMap.put("associatedMnc", associatedMnc);
		            ksession.dispose();
		            System.out.println("returninging responseMap map --->"+responseMap);

		return responseMap;
	}


}