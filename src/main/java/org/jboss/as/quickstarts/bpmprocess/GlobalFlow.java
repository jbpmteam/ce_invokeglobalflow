package org.jboss.as.quickstarts.bpmprocess;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

import java.util.UUID;

import com.avalonhc.as.facts.ClaimHistFreqDosUnit;
import com.avalonhc.as.facts.ClaimRequest;
import com.avalonhc.as.facts.EvaluateLabClaim;
import com.avalonhc.as.facts.EvaluateLabClaimResponse;
import com.avalonhc.as.facts.ResponseHeaderData;
import com.avalonhc.as.facts.ResponseLineData;
import com.avalonhc.as.facts.SecondaryCodes;
import java.util.logging.Logger; 


public class GlobalFlow {
	//public EvaluateLabClaimResponse start(ClaimObj p) throws ParseException {
	private final static Logger LOGGER = Logger.getLogger(GlobalFlow.class.getName());  
	public EvaluateLabClaimResponse start(EvaluateLabClaim evaluateLabClaim) throws ParseException,Exception {

		String path = System.getProperty("javaDefaultValue"); 
		 
		 LOGGER.info("Process Main environment variable::"+path);		
		KieServices ks = KieServices.Factory.get();
		KieContainer kContainer = ks.getKieClasspathContainer();
		KieSession ksession = kContainer.newKieSession("ksession-process");
		
		Map<String, Object> params = new HashMap<String, Object>();
         SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
         ClaimRequest claimRequest =new ClaimRequest();
         ClaimHistFreqDosUnit claimHistFreqDosUnit =new ClaimHistFreqDosUnit();

         LOGGER.info("healthpland:::"+evaluateLabClaim.getRequestHeader().getHealthPlanId());
         if(null!= evaluateLabClaim.getRequestHeader().getHealthPlanId() && !"".equals(evaluateLabClaim.getRequestHeader().getHealthPlanId()) && !"?".equals(evaluateLabClaim.getRequestHeader().getHealthPlanId())){
     		claimRequest.setHealthPlanId(evaluateLabClaim.getRequestHeader().getHealthPlanId().trim());
     		LOGGER.info("Health plan:::"+evaluateLabClaim.getRequestHeader().getHealthPlanId());
     		}
         if(null!= evaluateLabClaim.getRequestHeader().getUniqueMemberId() && !"".equals(evaluateLabClaim.getRequestHeader().getUniqueMemberId()) && !"?".equals(evaluateLabClaim.getRequestHeader().getUniqueMemberId())){
      		//claimRequest.setHealthPlanId(evaluateLabClaim.getRequestHeader().getUniqueMemberId());
      		LOGGER.info("UniqueMemberId:::"+evaluateLabClaim.getRequestHeader().getUniqueMemberId());
      		}
     	
    	        	
         if(null!=evaluateLabClaim.getRequestLine().get(0).getProcedureCode() && !"".equals(evaluateLabClaim.getRequestLine().get(0).getProcedureCode()) && !"?".equals(evaluateLabClaim.getRequestLine().get(0).getProcedureCode())){
	 		   claimRequest.setProcedureCode(evaluateLabClaim.getRequestLine().get(0).getProcedureCode().trim());
	 		   LOGGER.info("procedure code:::"+ evaluateLabClaim.getRequestLine().get(0).getProcedureCode());
	 		} 
    	         
         if(null!=evaluateLabClaim.getRequestLine().get(0).getFromDateOfService() && !"".equals(evaluateLabClaim.getRequestLine().get(0).getFromDateOfService()) && !"?".equals(evaluateLabClaim.getRequestLine().get(0).getFromDateOfService())){
	       	claimRequest.setFromDateOfService(evaluateLabClaim.getRequestLine().get(0).getFromDateOfService().toGregorianCalendar().getTime());
        	 //claimRequest.setFromDateOfService(evaluateLabClaim.getRequestLine().get(0).getFromDateOfService());
	        	 LOGGER.info("From Date::"+evaluateLabClaim.getRequestLine().get(0).getFromDateOfService());
	         }
    	      
	        	 LOGGER.info("DOB::"+evaluateLabClaim.getRequestHeader().getPatientDateOfBirth() );
	        	 LOGGER.info("primaryDiagnosis:::"+evaluateLabClaim.getRequestHeader().getPrimaryDiagnosisCode());
	        	 
	        	 if(null!=evaluateLabClaim.getRequestLine().get(0).getPlaceOfService() && !"".equals(evaluateLabClaim.getRequestLine().get(0).getPlaceOfService()) && !"?".equals(evaluateLabClaim.getRequestLine().get(0).getPlaceOfService())){
	 	        	   claimRequest.setPlaceOfService(evaluateLabClaim.getRequestLine().get(0).getPlaceOfService().trim());
	    	        	 LOGGER.info("PlaceOfService::::"+evaluateLabClaim.getRequestLine().get(0).getPlaceOfService());
	 	        	  }
    	        	 //setting PrimaryDiagnosisCode and DiagnosisCode in List for comparing with single column value in decision table
	        	 claimRequest.setError("NO");
	        	 List<String> diagnosisCodesList =new ArrayList<String>();
	        	 int DiagnosisCodePointersCount=0;
	        	
	        	 if(null== evaluateLabClaim.getRequestHeader().getPrimaryDiagnosisCode() || "".equals(evaluateLabClaim.getRequestHeader().getPrimaryDiagnosisCode()) || "?".equals(evaluateLabClaim.getRequestHeader().getPrimaryDiagnosisCode())){
	        		 if (null != evaluateLabClaim.getRequestLine().get(0).getDiagnosisCodePointers().get(0) && !"".equals(evaluateLabClaim.getRequestLine().get(0).getDiagnosisCodePointers().get(0))){ 
	   	        	  DiagnosisCodePointersCount =Integer.parseInt(evaluateLabClaim.getRequestLine().get(0).getDiagnosisCodePointers().get(0));
	        		 claimRequest.setDiagnosisCodePointers(DiagnosisCodePointersCount);
	        	 }
	        	 }
	        	 int count = evaluateLabClaim.getRequestHeader().getDiagnosisCodes().size(); 
	        	 
    	        	 LOGGER.info("Step 1 diagnosisCodesList ---->");
    	        	 
    	        	
    	        		if(null!= evaluateLabClaim.getRequestHeader().getPrimaryDiagnosisCode() && !"".equals(evaluateLabClaim.getRequestHeader().getPrimaryDiagnosisCode()) && !"?".equals(evaluateLabClaim.getRequestHeader().getPrimaryDiagnosisCode())){
    	      	        	 diagnosisCodesList.add(evaluateLabClaim.getRequestHeader().getPrimaryDiagnosisCode().trim());
    	      	        	 claimRequest.setPrimaryDiagnosisCode(evaluateLabClaim.getRequestHeader().getPrimaryDiagnosisCode().trim());
    	      	        	}
    	        		LOGGER.info("in for loop diagnosiscode list"+evaluateLabClaim.getRequestHeader().getDiagnosisCodes());
	  	        		LOGGER.info("in for loop the value of count1"+count);
    	  	        	if(null== evaluateLabClaim.getRequestHeader().getDiagnosisCodes() || "".equals(evaluateLabClaim.getRequestHeader().getDiagnosisCodes()) || "?".equals(evaluateLabClaim.getRequestHeader().getDiagnosisCodes())){
    	  	        		LOGGER.info("in for loop diagnosiscode list"+evaluateLabClaim.getRequestHeader().getDiagnosisCodes());
    	  	        		LOGGER.info("in for loop the value of count"+count);
    	  	        		for (int i = 0; i < count; i++) {
    	  	        			LOGGER.info("in for loop the value of i"+i);
    	  	        			diagnosisCodesList.add(evaluateLabClaim.getRequestHeader().getDiagnosisCodes().get(i).trim());
    	  	        			LOGGER.info("in for loop diagnosiscode list"+evaluateLabClaim.getRequestHeader().getDiagnosisCodes().get(i));
    	  	      		}
    	  	       
    	  	        	 
    	  	        	}
    	        		 LOGGER.info("Step 1 diagnosisCodesList ---->");
    	      	        	if(null!= evaluateLabClaim.getRequestHeader().getDiagnosisCodes() && !"".equals(evaluateLabClaim.getRequestHeader().getDiagnosisCodes())&& ! "?".equals(evaluateLabClaim.getRequestHeader().getDiagnosisCodes())){
    	      	        	// diagnosisCodesList.add(evaluateLabClaim.getRequestHeader().getDiagnosisCodes());
    	      	        		for(int  i=0;i<evaluateLabClaim.getRequestHeader().getDiagnosisCodes().size();i++){
    	      	        			LOGGER.info("diagnosisCodesList ---->"+evaluateLabClaim.getRequestHeader().getDiagnosisCodes().get(i));
    	      	        			diagnosisCodesList.add(evaluateLabClaim.getRequestHeader().getDiagnosisCodes().get(i).trim());
    	      	        		}
    	      	        		
    	      	        	}
    	      	        	LOGGER.info("diagnosisCodesList ---->"+diagnosisCodesList); 
    	      	        	claimRequest.setDiagnosisCodesList(diagnosisCodesList);
    	     	        	 
    	     	        	if(null!=evaluateLabClaim.getRequestHeader().getPatientGenderCode() && !"".equals(evaluateLabClaim.getRequestHeader().getPatientGenderCode()) && ! "?".equals(evaluateLabClaim.getRequestHeader().getPatientGenderCode()) ){
    	        	        	 LOGGER.info("Gender::::"+evaluateLabClaim.getRequestHeader().getPatientGenderCode());
    	        	        	 claimRequest.setPatientGenderCode(evaluateLabClaim.getRequestHeader().getPatientGenderCode().trim());
    	        	        	}
    	     	        	
    	     	        	
    	     	        	if(null!=evaluateLabClaim.getRequestHeader().getPatientDateOfBirth() && !"".equals(evaluateLabClaim.getRequestHeader().getPatientDateOfBirth()) && ! "?".equals(evaluateLabClaim.getRequestHeader().getPatientDateOfBirth()) ){ 
    	     	        		
    	     	        		claimRequest.setPatientDOB(evaluateLabClaim.getRequestHeader().getPatientDateOfBirth().toString());
    	     	        		 LOGGER.info("Patient DOB::::"+evaluateLabClaim.getRequestHeader().getPatientDateOfBirth());
    	     	        	}
    	     	        	else
    	     	        	{
    	     	        		claimRequest.setPatientDOB(null);
    	     	        	}
    	     	        	
    	     	        	if((evaluateLabClaim.getRequestLine().get(0).getUnits())!= 0 && !"".equals(evaluateLabClaim.getRequestLine().get(0).getUnits()) && !"?".equals(evaluateLabClaim.getRequestLine().get(0).getUnits())){
    	 	 	        	   claimRequest.setUnits(evaluateLabClaim.getRequestLine().get(0).getUnits());
    	 	    	        	 LOGGER.info("Units::::"+evaluateLabClaim.getRequestLine().get(0).getUnits());
    	 	 	        	  }
    	     	        	if(null!=evaluateLabClaim.getRequestHeader().getBusinessSectorCd() && !"".equals(evaluateLabClaim.getRequestHeader().getBusinessSectorCd()) && ! "?".equals(evaluateLabClaim.getRequestHeader().getBusinessSectorCd()) ){
   	        	        	 LOGGER.info("Business Sector cd::::"+evaluateLabClaim.getRequestHeader().getBusinessSectorCd());
   	        	        	
   	        	        	 claimRequest.setBussinessSectorCd(evaluateLabClaim.getRequestHeader().getBusinessSectorCd().trim());
   	        	        	}
    	     	        	
    	     	       	if(null!=evaluateLabClaim.getRequestHeader().getExcludeMembershipIndicator() && !"".equals(evaluateLabClaim.getRequestHeader().getExcludeMembershipIndicator()) && ! "?".equals(evaluateLabClaim.getRequestHeader().getExcludeMembershipIndicator()) ){
  	        	        	 LOGGER.info("ExcludeMembershipIndicator::::"+evaluateLabClaim.getRequestHeader().getExcludeMembershipIndicator());
  	        	        	
  	        	        	 claimRequest.setExcludeMembershipIndicator(evaluateLabClaim.getRequestHeader().getExcludeMembershipIndicator());
  	        	        	}
    	     	        	
    	     	        	
    	     	        	if(null!=evaluateLabClaim.getRequestHeader().getUniqueMemberId() && !"".equals(evaluateLabClaim.getRequestHeader().getUniqueMemberId()) && ! "?".equals(evaluateLabClaim.getRequestHeader().getUniqueMemberId()) ){
      	        	        	 LOGGER.info("uniquememberid::::"+evaluateLabClaim.getRequestHeader().getUniqueMemberId());
      	        	        	
      	        	        	 claimRequest.setUniqueMemberId(evaluateLabClaim.getRequestHeader().getUniqueMemberId().trim());
      	        	        	}
    	     	        	
    	     	        	    	     	        	
    	     	       	if(null!=evaluateLabClaim.getRequestHeader().getLineOfBusiness() && !"".equals(evaluateLabClaim.getRequestHeader().getLineOfBusiness()) )
	     	        	{
	     	        		claimRequest.setLineOfBusiness(evaluateLabClaim.getRequestHeader().getLineOfBusiness());
	     	        	}	
    	     	       	
    	     	      	     	        	
	     	        	if(null!=evaluateLabClaim.getRequestHeader().getHealthPlanGroupId() && !"".equals(evaluateLabClaim.getRequestHeader().getHealthPlanGroupId()) && !"?".equals(evaluateLabClaim.getRequestHeader().getHealthPlanGroupId())){
	     	        		claimRequest.setHealthPlanGroupId(evaluateLabClaim.getRequestHeader().getHealthPlanGroupId().trim());
	     	        	}
	     	        	
	     	        	
	     	        	if(null!=evaluateLabClaim.getRequestHeader().getBlueCardCode() && !"".equals(evaluateLabClaim.getRequestHeader().getBlueCardCode()) && !"?".equals(evaluateLabClaim.getRequestHeader().getBlueCardCode())){
	     	        		claimRequest.setBlueCardCode(evaluateLabClaim.getRequestHeader().getBlueCardCode().trim());
	     	        	}
	     	        	
	     	        	if(null!=evaluateLabClaim.getRequestHeader().getIdCardNumber() && !"".equals(evaluateLabClaim.getRequestHeader().getIdCardNumber()) && !"?".equals(evaluateLabClaim.getRequestHeader().getIdCardNumber())){
	     	        		claimRequest.setIdCardNumber(evaluateLabClaim.getRequestHeader().getIdCardNumber().trim());
	     	        	}
	     	        	
	     	        	 if(null!=evaluateLabClaim.getRequestLine() && !evaluateLabClaim.getRequestLine().isEmpty() && null!=evaluateLabClaim.getRequestLine().get(0).getPlaceOfService() && !"".equals(evaluateLabClaim.getRequestLine().get(0).getProcedureCode()) && !"?".equals(evaluateLabClaim.getRequestLine().get(0).getProcedureCode())){
	     	 	 		   claimRequest.setPlaceOfService(evaluateLabClaim.getRequestLine().get(0).getPlaceOfService().trim());
	     	 	 		} 
	     	        	
	     	        	 if(null!=evaluateLabClaim.getRequestLine() && !evaluateLabClaim.getRequestLine().isEmpty() && null!=evaluateLabClaim.getRequestLine().get(0).getInNetworkIndicator() && !"".equals(evaluateLabClaim.getRequestLine().get(0).getInNetworkIndicator()) && !"?".equals(evaluateLabClaim.getRequestLine().get(0).getInNetworkIndicator())){
 	     	 	 		   claimRequest.setInNetworkIndicator(evaluateLabClaim.getRequestLine().get(0).getInNetworkIndicator().trim());
 	     	 	 		} 
	     	        	 
	     	        	if(null!=evaluateLabClaim.getRequestLine() && !evaluateLabClaim.getRequestLine().isEmpty() && null!=evaluateLabClaim.getRequestLine().get(0).getOtherClaimEditorIndicator() && !"".equals(evaluateLabClaim.getRequestLine().get(0).getOtherClaimEditorIndicator()) && !"?".equals(evaluateLabClaim.getRequestLine().get(0).getOtherClaimEditorIndicator())){
	        	     	   claimRequest.setOtherClaimEditorIndicator(evaluateLabClaim.getRequestLine().get(0).getOtherClaimEditorIndicator().trim());
	        	     	}  	        	
            
	     	        	if(null!=evaluateLabClaim.getRequestHeader().getBillingProviderNpi() && !"".equals(evaluateLabClaim.getRequestHeader().getBillingProviderNpi()) && !"?".equals(evaluateLabClaim.getRequestHeader().getBillingProviderNpi())){
	     	        		claimRequest.setBillingProviderNpi(evaluateLabClaim.getRequestHeader().getBillingProviderNpi().trim());
	     	        	}
	     	        	
	     	        	if(null!=evaluateLabClaim.getRequestHeader().getRenderingProviderNpi() && !"".equals(evaluateLabClaim.getRequestHeader().getRenderingProviderNpi()) && !"?".equals(evaluateLabClaim.getRequestHeader().getRenderingProviderNpi())){
	     	        		claimRequest.setRenderingProviderNpi(evaluateLabClaim.getRequestHeader().getRenderingProviderNpi().trim());
	     	        	}
                 
				        if(null!=evaluateLabClaim.getRequestLine() && !evaluateLabClaim.getRequestLine().isEmpty() && null!=evaluateLabClaim.getRequestLine().get(0).getPaidDeniedIndicator() && !"".equals(evaluateLabClaim.getRequestLine().get(0).getPaidDeniedIndicator()) && !"?".equals(evaluateLabClaim.getRequestLine().get(0).getPaidDeniedIndicator()))
                         {
	                      claimRequest.setPaidDeniedIndicator(evaluateLabClaim.getRequestLine().get(0).getPaidDeniedIndicator().trim());
                        } 
				        
				    	//Add cobIndicator
    	     	       	if(null!=evaluateLabClaim.getRequestHeader().getCobIndicator() && !"".equals(evaluateLabClaim.getRequestHeader().getCobIndicator()) && ! "?".equals(evaluateLabClaim.getRequestHeader().getCobIndicator()) ){
  	        	        	 LOGGER.info("CobIndicator::::"+evaluateLabClaim.getRequestHeader().getCobIndicator());
  	        	        	
  	        	        	 claimRequest.setCobIndicator(evaluateLabClaim.getRequestHeader().getCobIndicator());
  	        	        }
    	     	       	
     	        	//Sample Response creation for global flow
    	     	        /*	UUID uuid = UUID.randomUUID();
    	     			    String randomUUIDString = uuid.toString();*/
    	     	        	
     	        	ResponseHeaderData responseHeaderData = new ResponseHeaderData();
     	        	
     	        	//LOGGER.info("randomUUIDString : : : "+randomUUIDString);
     	        	responseHeaderData.setCeTransactionId(evaluateLabClaim.getRequestHeader().getCeTransactionId());
     	        
     	        	responseHeaderData.setClaimNumber(evaluateLabClaim.getRequestHeader().getClaimNumber());
    	        	LOGGER.info("ClaimNumber::::"+evaluateLabClaim.getRequestHeader().getClaimNumber());	 
	            
     	        	EvaluateLabClaimResponse evaluateLabClaimResponse = new EvaluateLabClaimResponse();
     	        	SecondaryCodes secondaryCodes = new SecondaryCodes();
     	        	ResponseLineData responseLineData =new ResponseLineData();
     	        	
     	        	
     	        	evaluateLabClaimResponse.setResponseHeader(responseHeaderData);
     	        	evaluateLabClaimResponse.setResponseHeader(responseHeaderData);
     	        	
     	        	claimHistFreqDosUnit.setProcedureCode(claimRequest.getProcedureCode());
     	        	claimHistFreqDosUnit.setHealthPlanId(claimRequest.getHealthPlanId());
     	        	claimHistFreqDosUnit.setFromDateOfService(claimRequest.getFromDateOfService());
     	        	claimHistFreqDosUnit.setDiagnosisCode(claimRequest.getDiagnosisCode());
     	        	claimHistFreqDosUnit.setPatientGenderCode(claimRequest.getPatientGenderCode());
     	        	claimHistFreqDosUnit.setExcludeMembershipIndicator(claimRequest.getExcludeMembershipIndicator());

     	        	
     	        	if((evaluateLabClaim.getRequestLine().get(0).getUnits())!= 0 && !"".equals(evaluateLabClaim.getRequestLine().get(0).getUnits()) && !"?".equals(evaluateLabClaim.getRequestLine().get(0).getUnits())){
     	        		responseLineData.setApprovedServiceUnitCount(evaluateLabClaim.getRequestLine().get(0).getUnits());
     	        		
     	        	}
    	        	 
    	        	 params.put("claimReq", claimRequest);
    	        	 params.put("evaluateLabClaimResponse", evaluateLabClaimResponse);
    	        	 params.put("secondaryCodes", secondaryCodes);
    	        	 params.put("responseLineData", responseLineData);
    	        	 params.put("claimHistFreqDosUnit", claimHistFreqDosUnit);
    	        	 try{
		            ksession.startProcess("defaultPackage.GlobalFlow", params);
		            ksession.insert(evaluateLabClaim);
		            ksession.fireAllRules();	
		            
		            //if(null!=evaluateLabClaim.getRequestLine().get(0).getLineNumber()){
		            if(evaluateLabClaimResponse.getResponseLine()!=null && !evaluateLabClaimResponse.getResponseLine().isEmpty()){	
		            evaluateLabClaimResponse.getResponseLine().get(0).setLineNumber(evaluateLabClaim.getRequestLine().get(0).getLineNumber());
		            }
  	    	        	 LOGGER.info("getLineNumber::::"+evaluateLabClaim.getRequestLine().get(0).getLineNumber());
  	 	        	  //}  
  	    	         if(evaluateLabClaimResponse.getResponseLine()!=null && !evaluateLabClaimResponse.getResponseLine().isEmpty()){	
  	   		            evaluateLabClaimResponse.getResponseLine().get(0).setProcedureCode(evaluateLabClaim.getRequestLine().get(0).getProcedureCode());
  	   		            }
  	    	        	//LOGGER.info("ProcedureCode::::"+evaluateLabClaim.getRequestLine().get(0).getProcedureCode());	 
  	    	        	
		            ksession.dispose();
		            LOGGER.info("returninging responseMap map --->"+evaluateLabClaimResponse);
		            
    	        	 }catch(Exception ex){
        	        	 System.out.println("Exception in global flow"+ex.getMessage());
        	        	 throw ex;
        	         }

		            return evaluateLabClaimResponse;
	}


}