package org.jboss.as.quickstarts.bpmprocess;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


//import org.jboss.as.quickstarts.Util.DateUtility;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

import com.avalonhc.as.facts.ClaimObj;
import com.avalonhc.as.facts.ClaimRequest;

import java.util.logging.Logger; 

public class PxDxMatchingMain {
	
	private final static Logger LOGGER = Logger.getLogger(PxDxMatchingMain.class.getName()); 
	public Map<String,String>  start(ClaimObj p) throws ParseException {
		String path = System.getProperty("javaDefaultValue"); 
		 
		 LOGGER.info("Process Main environment variable::"+path);		
		KieServices ks = KieServices.Factory.get();
		KieContainer kContainer = ks.getKieClasspathContainer();
		KieSession ksession = kContainer.newKieSession("ksession-process");
		Map<String, Object> params = new HashMap<String, Object>();

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
         ClaimRequest claimRequest =new ClaimRequest();
         
         
         if(null!= p.getHealthPlanId() && !"".equals(p.getHealthPlanId())){
      		claimRequest.setHealthPlanId(p.getHealthPlanId());
      		LOGGER.info(p.getHealthPlanId());
      		}
      		LOGGER.info("Hello");

     	         LOGGER.info("Hello");
     	        	
     	         if(null!=p.getServiceLines().get(0).getProcedureCode() && !"".equals(p.getServiceLines().get(0).getProcedureCode())){
     	 		   claimRequest.setProcedureCode(p.getServiceLines().get(0).getProcedureCode());
     	 		   LOGGER.info("proceceur"+ p.getServiceLines().get(0).getProcedureCode());
     	 		} 
     	         
     	         if(null!=p.getServiceLines().get(0).getFromDateOfService() && !"".equals(p.getServiceLines().get(0).getFromDateOfService())){
     	        	claimRequest.setFromDateOfService(sdf.parse(p.getServiceLines().get(0).getFromDateOfService())); 
     	        	 LOGGER.info("date::"+p.getServiceLines().get(0).getFromDateOfService());
     	         }
 
    	        	 LOGGER.info("getPrimaryDiagnosisCode"+p.getPrimaryDiagnosisCode());
    	        	 LOGGER.info("getDiagnosisCodePointers"+p.getDiagnosisCodePointers());
    	        	 claimRequest.setError("No");
    	        	
    	        	 List<String> diagnosisCodesList =new ArrayList<String>();
    	        	 int count=p.getDiagnosisCodesList().size();
    	        	 int DiagnosisCodePointersCount =Integer.parseInt(p.getDiagnosisCodePointers());
    	        	 claimRequest.setDiagnosisCodePointers(DiagnosisCodePointersCount);
    	        	
    	  	        	if(null!= p.getPrimaryDiagnosisCode() && !"".equals(p.getPrimaryDiagnosisCode()) && !"?".equals(p.getPrimaryDiagnosisCode()))
    	  	        	{
    	  	        	
    	  	    
    	  	        		 claimRequest.setPrimaryDiagnosisCode(p.getPrimaryDiagnosisCode());
    	  	        		LOGGER.info(" primary diagnosis code is null"+claimRequest.getPrimaryDiagnosisCode());
    	  	        	}
    	  	        	
    	  	        	
    	  	        	LOGGER.info("in for loop diagnosiscode list"+p.getDiagnosisCodes());
	  	        		LOGGER.info("in for loop the value of count1"+count);
    	  	        	if(null== p.getDiagnosisCodes() || "".equals(p.getDiagnosisCodes()) || "?".equals(p.getDiagnosisCodes())){
    	  	        		LOGGER.info("in for loop diagnosiscode list"+p.getDiagnosisCodes());
    	  	        		LOGGER.info("in for loop the value of count"+count);
    	  	        		for (int i = 0; i < count; i++) {
    	  	        			LOGGER.info("in for loop the value of i"+i);
    	  	        			diagnosisCodesList.add(p.getDiagnosisCodesList().get(i).getDiagnosisCode());
    	  	        			LOGGER.info("in for loop diagnosiscode list"+p.getDiagnosisCodesList().get(i).getDiagnosisCode());
    	  	      		}
    	  	       
    	  	        	 
    	  	        	}
    	  	        	 claimRequest.setDiagnosisCodesList(diagnosisCodesList);
    	     	        
    	
    	  	        	 
    	  	        	 
    	  	        	for (int i = 0; i < count; i++) {
	  	        			LOGGER.info(diagnosisCodesList.get(i));
	  	      		}
    	  	        	
    	        	 LOGGER.info("Diagnosis code List"+p.getDiagnosisCodesList().size());
    	        	 
    	        	 LOGGER.info(claimRequest.getPrimaryDiagnosisCode());
    	        	
    	        	 params.put("claimReq", claimRequest);

		            ksession.startProcess("ClaimEditProcess.PxDxMatchingsubflow", params);
		            
		            ksession.insert(p);
		            ksession.fireAllRules();
		            String response=claimRequest.getDecision();
		            String associatedMPolicy=claimRequest.getAssociatedMPolicy();
		            String associatedMnc=claimRequest.getAssociatedMnc();
		            Map<String,String>  responseMap = new HashMap<String, String>();
		            responseMap.put("response", response);
		            responseMap.put("associatedMPolicy", associatedMPolicy);
		            responseMap.put("associatedMnc", associatedMnc);
		            ksession.dispose();
		            LOGGER.info("returninging responseMap map --->"+responseMap);

		return responseMap;
	}


}