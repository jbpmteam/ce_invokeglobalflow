package org.jboss.as.quickstarts.bpmprocess;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

import com.avalonhc.as.facts.ClaimRequest;


import com.avalonhc.as.facts.ClaimObj;

import java.util.logging.Logger; 

public class MESameClaim {
	private final static Logger LOGGER = Logger.getLogger(MESameClaim.class.getName()); 
	public Map<String,String> start(ClaimObj p) throws ParseException {
		String path = System.getProperty("javaDefaultValue"); 
		 
		 System.out.println("Process Main environment variable::"+path);		
		KieServices ks = KieServices.Factory.get();
		KieContainer kContainer = ks.getKieClasspathContainer();
		
		KieSession ksession = kContainer.newKieSession("ksession-process");
		
		
		Map<String, Object> params = new HashMap<String, Object>();
		
	
         SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
         ClaimRequest claimRequest =new ClaimRequest();
         
         LOGGER.info("healthpland"+p.getHealthPlanId());
     	claimRequest.setHealthPlanId(p.getHealthPlanId());
         LOGGER.info(p.getHealthPlanId());
    	        	
    	         LOGGER.info("Hello");
    	        	claimRequest.setProcedureCode(p.getServiceLines().get(0).getProcedureCode());
    	        	 LOGGER.info("proceceur"+p.getServiceLines().get(0).getProcedureCode());
    	        
    	        	claimRequest.setFromDateOfService(sdf.parse(p.getServiceLines().get(0).getFromDateOfService())); 
    	        	 LOGGER.info("date::"+p.getServiceLines().get(0).getFromDateOfService());

    	        	 
    	        	 params.put("claimReq", claimRequest);
    	        	 
    	        	 LOGGER.info(" before  calling ClaimRepositorys.MESameclaimProcess");

		            ksession.startProcess("claim.MESameClaimProcess", params);
		            ksession.insert(p);
		            ksession.fireAllRules();	
		            
		            LOGGER.info(" After  calling ClaimRepositorys.MESameclaimProcess");
		            
		            String associatedMPolicy=claimRequest.getAssociatedMPolicy();
		            String associatedMnc=claimRequest.getAssociatedMnc();		            
		            String response=claimRequest.getDecision();
		            
		            Map<String,String>  responseMap = new HashMap<String, String>();
		            responseMap.put("response", response);
		            responseMap.put("associatedMPolicy", associatedMPolicy);
		            responseMap.put("associatedMnc", associatedMnc);
		            ksession.dispose();
		            LOGGER.info("returninging responseMap map --->"+responseMap);

		return responseMap;

}
}


