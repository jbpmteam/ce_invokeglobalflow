package org.jboss.as.quickstarts.Util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Map;

import org.jboss.as.quickstarts.bpmprocess.ProcedureCodeLookupMain;
import org.junit.BeforeClass;
import org.junit.Test;

import com.avalonhc.as.facts.ClaimObj;
import com.avalonhc.as.facts.ClaimRequest;
import com.avalonhc.as.facts.ServiceLine;

public class ProcessJunitTest_ProcedureCodeLookUp {
	private static ProcedureCodeLookupMain ProcedureCodeLookupMain;
	private static ClaimObj claim;
	private static ClaimRequest claimRequest;
	private static ServiceLine ServiceLines;

	@BeforeClass
	public static void setUp() throws ParseException {
		
		ProcedureCodeLookupMain = new ProcedureCodeLookupMain();
		claim = new ClaimObj();
		claimRequest = new ClaimRequest();
		
		System.out.println(ProcedureCodeLookupMain);
		
		ArrayList<ServiceLine> serviceL = new ArrayList<ServiceLine>();
		claim.setHealthPlanId("401");
		ServiceLines = new ServiceLine();
		ServiceLines.setProcedureCode("81220");
		ServiceLines.setFromDateOfService("2016-06-21");
		
		serviceL.add(ServiceLines);
		claim.setServiceLines(serviceL);
		System.out.println("claim issss"+claim);

	}

	@Test
	public void test() throws ParseException {
		// fail("Not yet implemented");

		Map<String, String> resultMap = ProcedureCodeLookupMain.start(claim);
		System.out.println("claim obj is "+claim.getServiceLines());
		ArrayList<ServiceLine> serviceL = claim.getServiceLines();
		assertNotNull(claim);
		System.out.println("resultMap" + resultMap);
		
		assertEquals(resultMap.get("response"), "approved");

	}

}
