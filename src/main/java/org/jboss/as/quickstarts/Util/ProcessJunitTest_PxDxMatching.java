package org.jboss.as.quickstarts.Util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Map;

import org.jboss.as.quickstarts.bpmprocess.PxDxMatchingMain;
import org.junit.BeforeClass;
import org.junit.Test;

import com.avalonhc.as.facts.ClaimObj;
import com.avalonhc.as.facts.ClaimRequest;
import com.avalonhc.as.facts.ServiceLine;

public class ProcessJunitTest_PxDxMatching {
	private static PxDxMatchingMain pxdxMatchingMain;
	private static ClaimObj claim;
	private static ClaimRequest claimRequest;
	private static ServiceLine ServiceLines;

	@BeforeClass
	public static void setUp() throws ParseException {
		
		pxdxMatchingMain = new PxDxMatchingMain();
		claim = new ClaimObj();
		claimRequest = new ClaimRequest();
		
		System.out.println(pxdxMatchingMain);
		
		ArrayList<ServiceLine> serviceL = new ArrayList<ServiceLine>();
		
		claim.setHealthPlanId("401");
		
		//claim.setPatientDOB("1947-06-05");
		//claim.setDiagnosisCodes("F03.90-F03.91");
		claim.setPrimaryDiagnosisCode("Y99.1");
		
		//claim.setPatientGenderCode("M");
		ServiceLines = new ServiceLine();
		ServiceLines.setProcedureCode("12345");
		ServiceLines.setFromDateOfService("2016-06-21");
		
		serviceL.add(ServiceLines);
		claim.setServiceLines(serviceL);
		System.out.println("claim issss"+claim);

	}

	@Test
	public void test() throws ParseException {
		// fail("Not yet implemented");

		Map<String, String> resultMap = pxdxMatchingMain.start(claim);
		System.out.println("claim obj is "+claim.getServiceLines());
		ArrayList<ServiceLine> serviceL = claim.getServiceLines();
		assertNotNull(claim);
		System.out.println("resultMap" + resultMap);
		
		assertEquals(resultMap.get("response"), "D11R");

	}

}
