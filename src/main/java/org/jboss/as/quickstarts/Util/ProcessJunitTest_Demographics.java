package org.jboss.as.quickstarts.Util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Map;

import org.jboss.as.quickstarts.bpmprocess.DemographicsMain;
import org.junit.BeforeClass;
import org.junit.Test;

import com.avalonhc.as.facts.ClaimObj;
import com.avalonhc.as.facts.ClaimRequest;
import com.avalonhc.as.facts.ServiceLine;

public class ProcessJunitTest_Demographics {
	private static DemographicsMain demographicsMain;
	private static ClaimObj claim;
	private static ClaimRequest claimRequest;
	private static ServiceLine ServiceLines;

	@BeforeClass
	public static void setUp() throws ParseException {
		
		demographicsMain = new DemographicsMain();
		claim = new ClaimObj();
		claimRequest = new ClaimRequest();
		
		System.out.println(demographicsMain);
		
		ArrayList<ServiceLine> serviceL = new ArrayList<ServiceLine>();
		claim.setHealthPlanId("401");
		claim.setPatientDOB("1947-06-05");
		claim.setDiagnosisCodes("F03.90-F03.91");
		claim.setPatientGenderCode("M");
		ServiceLines = new ServiceLine();
		ServiceLines.setProcedureCode("87511");
		ServiceLines.setFromDateOfService("2016-06-21");
		
		serviceL.add(ServiceLines);
		claim.setServiceLines(serviceL);
		System.out.println("claim issss"+claim);

	}

	@Test
	public void test() throws ParseException {
		// fail("Not yet implemented");

	/*	Map<String, String> resultMap = demographicsMain.start(claim);
		System.out.println("claim obj is "+claim.getServiceLines());
		ArrayList<ServiceLine> serviceL = claim.getServiceLines();
		assertNotNull(claim);
		System.out.println("resultMap" + resultMap);
		
		assertEquals(resultMap.get("response"), "D04R");*/

	}

}
