package org.jboss.as.quickstarts.Util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Map;

import org.jboss.as.quickstarts.bpmprocess.PrimaryRequiredMain;
import org.junit.BeforeClass;
import org.junit.Test;

import com.avalonhc.as.facts.ClaimObj;
import com.avalonhc.as.facts.ClaimRequest;
import com.avalonhc.as.facts.ServiceLine;

public class ProcessJunitTest_PxCodeCompatibility {
	private static PrimaryRequiredMain pxcodeCompatibility;
	private static ClaimObj claim;
	private static ClaimRequest claimRequest;
	private static ServiceLine ServiceLines;

	@BeforeClass
	public static void setUp() throws ParseException {
		
		pxcodeCompatibility = new PrimaryRequiredMain();
		claim = new ClaimObj();
		claimRequest = new ClaimRequest();
		
		System.out.println(pxcodeCompatibility);
		
		ArrayList<ServiceLine> serviceL = new ArrayList<ServiceLine>();
		claim.setHealthPlanId("401");
		ServiceLines = new ServiceLine();
		ServiceLines.setProcedureCode("88341");
		ServiceLines.setFromDateOfService("2016-06-21");
		
		serviceL.add(ServiceLines);
		claim.setServiceLines(serviceL);
		System.out.println("claim issss"+claim);

	}

	@Test
	public void test() throws ParseException {
		// fail("Not yet implemented");

		Map<String, String> resultMap = pxcodeCompatibility.start(claim);
		System.out.println("claim obj is "+claim.getServiceLines());
		ArrayList<ServiceLine> serviceL = claim.getServiceLines();
		assertNotNull(claim);
		System.out.println("resultMap" + resultMap);
		
		assertEquals(resultMap.get("response"), "D10R");

	}

}
