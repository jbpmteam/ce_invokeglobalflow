package org.jboss.as.quickstarts.Util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Map;

import org.jboss.as.quickstarts.bpmprocess.PlaceOfServiceMain;
import org.junit.BeforeClass;
import org.junit.Test;

import com.avalonhc.as.facts.ClaimObj;
import com.avalonhc.as.facts.ClaimRequest;
import com.avalonhc.as.facts.ServiceLine;

public class ProcessJunitTest_PlaceOfService {
	private static PlaceOfServiceMain demographicsMain;
	private static ClaimObj claim;
	private static ClaimRequest claimRequest;
	private static ServiceLine ServiceLines;

	@BeforeClass
	public static void setUp() throws ParseException {
		
		demographicsMain = new PlaceOfServiceMain();
		claim = new ClaimObj();
		claimRequest = new ClaimRequest();
		
		//claimRequest.setCalculatedAge(calculatedAge);
		
		System.out.println(demographicsMain);
		
		ArrayList<ServiceLine> serviceL = new ArrayList<ServiceLine>();
		claim.setHealthPlanId("401");
		//claim.setPatientDOB("1974-06-05");
		ServiceLines = new ServiceLine();
		/*ServiceLines.setProcedureCode("87449");
		ServiceLines.setEffectiveStartDate("2016-06-21");
		ServiceLines.setPlaceOfService("11");*/
		
		ServiceLines.setProcedureCode("83037");
		ServiceLines.setFromDateOfService("2016-06-21");
		ServiceLines.setPlaceOfService("13");
		serviceL.add(ServiceLines);
		claim.setServiceLines(serviceL);
		System.out.println("claim issss"+claim);

	}

	@Test
	public void test() throws ParseException {
		// fail("Not yet implemented");

		Map<String, String> resultMap = demographicsMain.start(claim);
		System.out.println("claim obj is "+claim.getServiceLines());
		ArrayList<ServiceLine> serviceL = claim.getServiceLines();
		assertNotNull(claim);
		assertEquals(resultMap.get("response"), "D09R");
		System.out.println("resultMap" + resultMap);
		/*
		 * for (ServiceLine service : serviceL) {
		 * System.out.println("for each "+service.getResponseCode());
		 * assertEquals("approved",service.getResponseCode()); }
		 * System.out.println("hiii"+serviceL.get(0).getResponseCode());
		 * System.out.println(""+serviceL.size());
		 * System.out.println(""+serviceL.get(2));
		 * System.out.println(""+serviceL.get(3));
		 */

	}

}
