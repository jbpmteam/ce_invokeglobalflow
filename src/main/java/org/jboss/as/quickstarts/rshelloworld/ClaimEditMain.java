package org.jboss.as.quickstarts.rshelloworld;

import java.text.ParseException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import org.jboss.as.quickstarts.bpmprocess.GlobalFlow;

import com.avalonhc.as.facts.EvaluateLabClaim;
import com.avalonhc.as.facts.EvaluateLabClaimResponse;

@WebService(targetNamespace = "http://service.com.org/", portName = "ClaimEditPort", serviceName = "ClaimEditMain")
public class ClaimEditMain {

	/*@WebMethod(operationName = "procedureCodeLookup", action = "urn:procedureCodeLookup")
	@WebResult(name = "return")
	public ClaimObj procedureCodeLookup(ClaimObj claim) throws ParseException {

		ProcedureCodeLookupMain processMain = new ProcedureCodeLookupMain();
		Map<String, String> response = processMain.start(claim);

		claim.getServiceLines().get(0).getDecisionCodesListResponse().get(0)
				.setDecisionCode(response.get("response"));
		claim.getServiceLines().get(0).getDecisionCodesListResponse().get(0)
				.setPolicyNecessityCriteria(response.get("associatedMnc"));
		claim.getServiceLines().get(0).getDecisionCodesListResponse().get(0)
				.setPolicyTag(response.get("associatedMPolicy"));
		return claim;

	}
*/
	/*@WebMethod(operationName = "experimentalAndInvestigates", action = "urn:experimentalAndInvestigate")
	public ClaimObj experimentalAndInvestigate(ClaimObj claim)
			throws ParseException {

		ExperimentAndInvestigateMain expAndInvestObj = new ExperimentAndInvestigateMain();
		Map<String, String> response = expAndInvestObj.start(claim);

		claim.getServiceLines().get(0).getDecisionCodesListResponse().get(0)
				.setDecisionCode(response.get("response"));
		claim.getServiceLines().get(0).getDecisionCodesListResponse().get(0)
				.setPolicyNecessityCriteria(response.get("associatedMnc"));
		claim.getServiceLines().get(0).getDecisionCodesListResponse().get(0)
				.setPolicyTag(response.get("associatedMPolicy"));
		return claim;

	}*/

	
	
	/*@WebMethod(operationName = "demographicsCheck", action = "urn:demographicsCheck" )
	@WebResult(name = "return")
public EvaluateLabClaimResponse  demographicsCheck(@WebParam(partName="parameter",name="EvaluateLabClaim") EvaluateLabClaim claim) throws ParseException {
		DemographicsMain demographicsMain = new DemographicsMain();
		EvaluateLabClaimResponse evaluateLabClaimResponse = demographicsMain.start(claim);

		return evaluateLabClaimResponse;

	}*/
/*
	@WebMethod(operationName = "placeOfService", action = "urn:placeOfService")
	@WebResult(name = "return")
	public ClaimObj placeOfService(ClaimObj claim) throws ParseException {

		PlaceOfServiceMain placeOfServiceMain = new PlaceOfServiceMain();
		Map<String, String> response = placeOfServiceMain.start(claim);

		claim.getServiceLines().get(0).getDecisionCodesListResponse().get(0)
				.setDecisionCode(response.get("response"));
		claim.getServiceLines().get(0).getDecisionCodesListResponse().get(0)
				.setPolicyNecessityCriteria(response.get("associatedMnc"));
		claim.getServiceLines().get(0).getDecisionCodesListResponse().get(0)
				.setPolicyTag(response.get("associatedMPolicy"));
		return claim;

	}

	@WebMethod(operationName = "primaryRequired", action = "urn:primaryRequired")
	@WebResult(name = "return")
	public ClaimObj primaryRequired(ClaimObj claim) throws ParseException {

		System.out.println("Starting the  primaryRequired  Service");
		PrimaryRequiredMain primaryRequiredMain = new PrimaryRequiredMain();
		Map<String, String> response = primaryRequiredMain.start(claim);

		System.out.println("After calling  the  start method ");
		claim.getServiceLines().get(0).getDecisionCodesListResponse().get(0)
				.setDecisionCode(response.get("response"));
		claim.getServiceLines().get(0).getDecisionCodesListResponse().get(0)
				.setPolicyNecessityCriteria(response.get("associatedMnc"));
		claim.getServiceLines().get(0).getDecisionCodesListResponse().get(0)
				.setPolicyTag(response.get("associatedMPolicy"));
		return claim;

	}

	@WebMethod(operationName = "pxdxMatching", action = "urn:pxdxMatching")
	@WebResult(name = "return")
	public ClaimObj pxdxMatching(ClaimObj claim) throws ParseException {

		PxDxMatchingMain pxDxMatchingMain = new PxDxMatchingMain();
		Map<String, String> response = pxDxMatchingMain.start(claim);

		claim.getServiceLines().get(0).getDecisionCodesListResponse().get(0)
				.setDecisionCode(response.get("response"));
		claim.getServiceLines().get(0).getDecisionCodesListResponse().get(0)
				.setPolicyNecessityCriteria(response.get("associatedMnc"));
		claim.getServiceLines().get(0).getDecisionCodesListResponse().get(0)
				.setPolicyTag(response.get("associatedMPolicy"));
		return claim;

	}

	@WebMethod(operationName = "meSameClaim", action = "urn:meSameClaim")
	@WebResult(name = "return")
	public ClaimObj meSameClaim(ClaimObj claim) throws ParseException {

		MESameClaim mESameClaim = new MESameClaim();
		Map<String, String> response = mESameClaim.start(claim);

		claim.getServiceLines().get(0).getDecisionCodesListResponse().get(0)
				.setDecisionCode(response.get("response"));
		claim.getServiceLines().get(0).getDecisionCodesListResponse().get(0)
				.setPolicyNecessityCriteria(response.get("associatedMnc"));
		claim.getServiceLines().get(0).getDecisionCodesListResponse().get(0)
				.setPolicyTag(response.get("associatedMPolicy"));
		return claim;

	}

	@WebMethod(operationName = "meFirstCome", action = "urn:meFirstCome")
	@WebResult(name = "return")
	public ClaimObj meFirstCome(ClaimObj claim) throws ParseException {

		MEFirstComeMain mEFirstComeMain = new MEFirstComeMain();
		Map<String, String> response = mEFirstComeMain.start(claim);

		claim.getServiceLines().get(0).getDecisionCodesListResponse().get(0)
				.setDecisionCode(response.get("response"));
		claim.getServiceLines().get(0).getDecisionCodesListResponse().get(0)
				.setPolicyNecessityCriteria(response.get("associatedMnc"));
		claim.getServiceLines().get(0).getDecisionCodesListResponse().get(0)
				.setPolicyTag(response.get("associatedMPolicy"));
		return claim;

	}

	@WebMethod(operationName = "wholesaleExclusions", action = "urn:wholesaleExclusions")
	@WebResult(name = "return")
	public ClaimObj wholesaleExclusions(ClaimObj claim) throws ParseException {

		WholesaleExclusionsMain wholesaleExclusionsMain = new WholesaleExclusionsMain();
		Map<String, String> response = wholesaleExclusionsMain.start(claim);

		claim.getServiceLines().get(0).getDecisionCodesListResponse().get(0)
				.setDecisionCode(response.get("response"));

		return claim;

	}

	@WebMethod(operationName = "exclusions", action = "urn:exclusions")
	@WebResult(name = "return")
	public ClaimObj exclusions(ClaimObj claim) throws ParseException {

		ExclusionsMain exclusionsMain = new ExclusionsMain();
		Map<String, String> response = exclusionsMain.start(claim);

		claim.getServiceLines().get(0).getDecisionCodesListResponse().get(0)
				.setDecisionCode(response.get("response"));
		claim.getServiceLines().get(0).getDecisionCodesListResponse().get(0)
				.setPolicyNecessityCriteria(response.get("associatedMnc"));
		claim.getServiceLines().get(0).getDecisionCodesListResponse().get(0)
				.setPolicyTag(response.get("associatedMPolicy"));

		return claim;

	}
*/
	@WebMethod(operationName = "globalFlow", action = "urn:globalFlow" )
	@WebResult(name = "return")
public EvaluateLabClaimResponse  globalFlow(@WebParam(partName="parameter",name="EvaluateLabClaim") EvaluateLabClaim claim) throws ParseException,Exception {
		GlobalFlow globalFlow = new GlobalFlow();
		EvaluateLabClaimResponse evaluateLabClaimResponse = globalFlow.start(claim);

		return evaluateLabClaimResponse;

	}

}